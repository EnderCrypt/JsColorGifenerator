package net.ddns.endercrypt.javascript;

import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

@Deprecated
public class CompiledJavascript
{
	private ScriptEngine engine;
	private CompiledScript script;

	private Bindings bindings;
	private String code;

	protected CompiledJavascript(ScriptEngine engine, String code) throws ScriptException
	{
		this.engine = engine;
		this.script = ((Compilable) engine).compile(code);

		this.bindings = engine.getBindings(ScriptContext.GLOBAL_SCOPE);
		this.code = code;
	}

	public ScriptEngine getEngine()
	{
		return engine;
	}

	public Bindings bindings()
	{
		return bindings;
	}

	public String getCode()
	{
		return code;
	}

	public Object eval() throws ScriptException
	{
		try
		{
			return script.eval(bindings);
		}
		catch (NullPointerException | ClassCastException e)
		{
			StringBuilder sb = new StringBuilder();
			sb.append(e.getClass().getSimpleName());

			if (e.getMessage() != null)
			{
				sb.append(": ");
				sb.append(e.getMessage());
			}

			throw new ScriptException(sb.toString());
		}
		catch (ScriptException e)
		{
			throw e;
		}
	}
}
