package net.ddns.endercrypt.javascript;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

@Deprecated
public class JavascriptEngine
{
	private static ScriptEngineManager scriptEngineManager = new ScriptEngineManager();

	private ScriptEngine javascript;

	@Deprecated
	private Bindings bindings;

	@Deprecated
	private String errorString;

	public JavascriptEngine()
	{
		javascript = scriptEngineManager.getEngineByName("javascript");

		bindings = javascript.getBindings(ScriptContext.ENGINE_SCOPE);
		bindings.put("print", (Consumer<Object>) this::js_print);
	}

	public CompiledJavascript compile(String code) throws ScriptException
	{
		return new CompiledJavascript(this.javascript, code);
	}

	@Deprecated
	public void evaluate(String code) throws ScriptException
	{
		errorString = null;

		try
		{
			try
			{
				javascript.eval(code);
			}
			catch (NullPointerException | ClassCastException e)
			{
				StringBuilder sb = new StringBuilder();
				sb.append(e.getClass().getSimpleName());

				if (e.getMessage() != null)
				{
					sb.append(": ");
					sb.append(e.getMessage());
				}

				throw new ScriptException(sb.toString());
			}
		}
		catch (ScriptException e)
		{
			errorString = e.getMessage();
			throw e;
		}
	}

	public Bindings bindings()
	{
		return bindings;
	}

	@Deprecated
	public Optional<String> getErrorString()
	{
		return Optional.ofNullable(errorString);
	}

	public void js_print(Object object)
	{
		System.out.println("JS print: " + Objects.toString(object));
	}
}
