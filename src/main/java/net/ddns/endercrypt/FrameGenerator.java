package net.ddns.endercrypt;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.script.Bindings;
import javax.script.CompiledScript;
import javax.script.ScriptException;

public class FrameGenerator implements Callable<BufferedImage>
{
	private CompiledScript script;
	private Bindings bindings;

	private int width;
	private int height;
	private int frame;

	private int r = 0;
	private int g = 0;
	private int b = 0;

	public FrameGenerator(CompiledScript script, Bindings bindings, int width, int height, int frame)
	{
		this.script = script;
		this.bindings = bindings;

		this.width = width;
		this.height = height;
		this.frame = frame;
	}

	private void setJavascript()
	{
		Function<Number, Integer> colorRange = (number) -> {
			double value = number.doubleValue();
			if (value > 255)
			{
				value = 255;
			}
			if (value < 0)
			{
				value = 0;
			}
			return (int) Math.round(value);
		};

		bindings.put("width", width);
		bindings.put("height", height);
		bindings.put("frame", frame);

		bindings.put("setRed", (Consumer<Number>) (red) -> {
			r = colorRange.apply(red);
		});
		bindings.put("setGreen", (Consumer<Number>) (green) -> {
			g = colorRange.apply(green);
		});
		bindings.put("setBlue", (Consumer<Number>) (blue) -> {
			b = colorRange.apply(blue);
		});
	}

	@Override
	public BufferedImage call() throws Exception
	{
		BufferedImage surface = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		int x = 0;
		int y = 0;
		try
		{
			while (true)
			{
				// prepare pixel
				r = 0;
				g = 0;
				b = 0;

				// set values
				setJavascript();
				bindings.put("x", x);
				bindings.put("y", y);

				// execute javascript
				script.eval(bindings);

				// set surface pixel
				/*
				if ((r != 99) || (g != 0) || (b != 0))
				{
					throw new IllegalArgumentException(r + ", " + g + ", " + b);
				}
				*/
				surface.setRGB(x, y, new Color(r, g, b).getRGB());

				// increment position
				x++;
				if (x >= width)
				{
					x = 0;
					y++;
					if (y >= height)
					{
						break;
					}
				}
			}
		}
		catch (ScriptException e)
		{
			System.err.println("JS error: " + e.getMessage());
			System.exit(-4);
		}

		System.out.println("Finished frame " + frame);
		return surface;
	}
}