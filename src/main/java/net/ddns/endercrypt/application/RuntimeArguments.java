package net.ddns.endercrypt.application;

import org.kohsuke.args4j.Option;

public class RuntimeArguments // -delay 50 -frames 20 -height 100 -width 100 -threads 5 -loop -script sample.js
{
	@Option(name = "-threads", aliases = { "-t" }, required = false, usage = "the amount of threads to use")
	private int threads = 3;

	public int getThreads()
	{
		return threads;
	}

	@Option(name = "-width", aliases = { "-w" }, required = true, usage = "the width of the gif")
	private int width;

	public int getWidth()
	{
		return width;
	}

	@Option(name = "-height", aliases = { "-h" }, required = true, usage = "the height of the gif")
	private int height;

	public int getHeight()
	{
		return height;
	}

	@Option(name = "-frames", aliases = { "-f" }, required = true, usage = "the amount of frames to give the gif")
	private int frames;

	public int getFrames()
	{
		return frames;
	}

	@Option(name = "-delay", aliases = { "-d" }, required = true, usage = "the delay between each frame in milliseconds")
	private int delay;

	public int getDelay()
	{
		return delay;
	}

	@Option(name = "-loop", aliases = { "-l" }, required = false, usage = "loop the gif?")
	private boolean loop = false;

	public boolean isLoop()
	{
		return loop;
	}

	@Option(name = "-script", aliases = { "-s" }, required = true, usage = "the javascript to use")
	private String script;

	public String getScript()
	{
		return script;
	}
}