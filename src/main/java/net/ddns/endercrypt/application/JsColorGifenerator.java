package net.ddns.endercrypt.application;

import java.io.File;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import net.ddns.endercrypt.Main;

public class JsColorGifenerator
{
	public static void main(String[] args)
	{
		System.out.println(JsColorGifenerator.class.getSimpleName() + " by EnderCrypt");

		RuntimeArguments runtimeArguments = new RuntimeArguments();

		CmdLineParser parser = new CmdLineParser(runtimeArguments);
		try
		{
			parser.parseArgument(args);
		}
		catch (CmdLineException e)
		{
			File file = new File(JsColorGifenerator.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			System.err.println("Error: " + e.getMessage());
			System.err.println("java -jar " + file.getName() + " [Options]");
			System.err.println("--{ args }--");
			parser.printUsage(System.err);
			System.exit(-1);
		}
		catch (Exception e)
		{
			System.err.println("Fatal exception parsing command line arguments!");
			e.printStackTrace();
			System.exit(-2);
		}

		try
		{
			Main.main(runtimeArguments);
		}
		catch (Exception e)
		{
			System.err.println("Fatal application exception!");
			e.printStackTrace();
			System.exit(-3);
		}
	}
}
