package net.ddns.endercrypt;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.squareup.gifencoder.GifEncoder;
import com.squareup.gifencoder.ImageOptions;

import net.ddns.endercrypt.application.RuntimeArguments;

public class Main
{
	private static ScriptEngineManager scriptEngineManager = new ScriptEngineManager();

	// private static final JavascriptEngine javascriptEngine = new JavascriptEngine();

	private static final Path outputDirectory = Paths.get("output");

	private static final DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH_mm_ss");

	private static ExecutorService executorService;

	public static void main(RuntimeArguments args) throws ScriptException, IOException, InterruptedException, ExecutionException
	{
		// threads
		executorService = Executors.newFixedThreadPool(args.getThreads());

		// script
		String script = new String(Files.readAllBytes(Paths.get(args.getScript())));

		ScriptEngine javascript = scriptEngineManager.getEngineByName("javascript");
		CompiledScript compiledScript = ((Compilable) javascript).compile(script);

		System.out.println("Generating frames...");
		List<Future<BufferedImage>> futureFrames = new ArrayList<>();
		for (int i = 0; i < args.getFrames(); i++)
		{
			// CompiledJavascript js = javascriptEngine.compile(script);
			Bindings bindings = javascript.createBindings();
			Future<BufferedImage> future = executorService.submit(new FrameGenerator(compiledScript, bindings, args.getWidth(), args.getHeight(), i + 1));
			futureFrames.add(future);
		}

		BufferedImage[] frames = new BufferedImage[args.getFrames()];
		{
			int i = 0;
			for (Future<BufferedImage> future : futureFrames)
			{
				frames[i] = future.get();
				i++;
			}
		}

		System.out.println("Assembling gif...");
		Files.createDirectories(outputDirectory);
		Path outputFile = outputDirectory.resolve(dateFormat.format(new Date()) + ".gif");

		// write gif
		ImageOptions options = new ImageOptions();
		options.setDelay(args.getDelay(), TimeUnit.MILLISECONDS);

		try (BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFile.toFile())))
		{
			// gif encoder
			GifEncoder gifEncoder = new GifEncoder(outputStream, args.getWidth(), args.getHeight(), args.isLoop() ? 0 : 1);

			// frames
			for (int i = 0; i < frames.length; i++)
			{
				BufferedImage frame = frames[i];

				int[] pixels = ((DataBufferInt) frame.getRaster().getDataBuffer()).getData();
				gifEncoder.addImage(pixels, args.getWidth(), options);
			}

			// done
			gifEncoder.finishEncoding();
		}

		// write script
		Path scriptOutputFile = outputDirectory.resolve(dateFormat.format(new Date()) + ".txt");
		Files.write(scriptOutputFile, script.getBytes());

		// stop executor
		executorService.shutdown();

		// done
		System.out.println("Done!");
	}
}
